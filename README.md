# ansible-role-gitlab-runner

Setup gitlab-runner with ansible.

## playbook examples

docker executor(default):

    - include_role:
        name: gitlab-runner

docker+machine executor and openstack driver:

    - include_role:
        name: gitlab-runner
        vars_from: docker-machine/openstack

docker+machine executor and rackspace driver:

    - include_role:
        name: gitlab-runner
        vars_from: docker-machine/rackspace

You need to privide registration token via cli:

    ./main.yml -e REGISTRATION_TOKEN=<YOUR-TOKEN>

which you can find at gitlab repo -> Settings -> CI/CD -> Runners
To override more vars, refer to:

- [defaults/main.yml](defaults/main.yml)
- [vars/](vars/)
- [templates/gitlab-runner-register.sh](templates/gitlab-runner-register.sh)

## Register same runner to multiple repo/group

    - name: install gitlab-runner without register
      include_role:
        name: gitlab-runner
        tasks_from: install

    - name: register gitlab-runner to multiple repo/group
      include_role:
        name: gitlab-runner
        tasks_from: register
      vars:
        CI_SERVER_URL: "{{url}}"
        REGISTRATION_TOKEN: "{{token}}"
        RUNNER_TAG_LIST: "{{tags}}"
      loop:
        - url: https://gitlab.com/
          token: XXXXX
          tags: docker,private
        - url: https://gitlab.example.com/
          token: FOOBAR
          tags: docker,private,example

